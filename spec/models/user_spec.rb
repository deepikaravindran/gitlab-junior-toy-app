require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { User.new(name: 'Example User', email: 'user@example.com', password: 'foobar', password_confirmation: 'foobar') }
  let(:valid_addresses) { %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn] }
  let(:invalid_address) { %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com] }

  it 'is valid' do
    expect(user).to be_valid
  end

  it 'has a name' do
    user.name = '      '
    expect(user).not_to be_valid
  end

  it 'has an email' do
    user.email = '   '
    expect(user).not_to be_valid
  end

  it 'does not have a too long name' do
    user.name = 'a' * 51
    expect(user).not_to be_valid
  end

  it 'does not have a too long email' do
    user.email = 'a' * 244 + '@example.com'
    expect(user).not_to be_valid
  end

  it 'accepts an email with valid format' do
    valid_addresses.each do |valid_address|
      user.email = valid_address
      expect(user).to be_valid, "#{valid_address.inspect} should be valid"
    end
  end

  it 'does not accept an email with valid format' do
    invalid_address.each do |valid_address|
      user.email = valid_address
      expect(user).not_to be_valid, "#{valid_address.inspect} should be invalid"
    end
  end

  it 'has a unique email address' do
    duplicate_user = user.dup
    user.save
    expect(duplicate_user).not_to be_valid
  end

  it 'has a password' do
    user.password = user.password_confirmation = ''
    expect(user).not_to be_valid
  end

  it 'has a password with minimum length' do
    user.password = user.password_confirmation = 'a' * 5
    expect(user).not_to be_valid
  end

  describe '#authenticated?' do
    it 'returns false for a user with nil digest' do
      expect(user.authenticated?(:remember, '')).to be_falsey
    end
  end
end
