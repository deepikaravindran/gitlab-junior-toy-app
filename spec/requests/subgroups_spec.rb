require 'rails_helper'

RSpec.describe "Subgroups", type: :request do
  let(:user) { create(:user) }
  let!(:top_level_group) { create(:group, owner: user) }
  let(:invalid_post_params) do
    { group: { name: '', description: 'description'  } }
  end

  let(:valid_post_params) do
    { group: { name: 'group_name', description: 'description'  } }
  end

  describe 'POST /groups/:group_id/subgroups' do
    context 'when not logged in' do
      it 'redirects to index' do
        post group_subgroups_path(top_level_group.id), params: valid_post_params
        expect(response).to redirect_to(login_url)
      end
    end

    context 'when logged in' do
      before do
        log_in_as(user)
      end

      it 'does not save with invalid params' do
        expect { post group_subgroups_path(top_level_group.id), params: invalid_post_params }.to_not change { Group.count }
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'saves with valid params' do
        expect { post group_subgroups_path(top_level_group.id), params: valid_post_params }.to change { Group.count }
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(group_path(Group.last.id))
      end
    end
  end
end

def log_in_as(user, password: 'password', remember_me: '1')
  post login_path, params: { session: { email: user.email,
    password: password,
    remember_me: remember_me } }
end
