class Group < ApplicationRecord
  delegate :url_helpers, to: 'Rails.application.routes'

  belongs_to :parent_group, class_name: 'Group', optional: true
  belongs_to :owner, class_name: 'User', foreign_key: 'owner_id'
  has_many :sub_groups, class_name: 'Group', foreign_key: 'parent_group_id', dependent: :destroy

  validates :name, presence: true
  validates :description, presence: true
  validates_with GroupOwnerValidator

  def full_path
    path = ["<strong>#{self.name}</strong>"]
    ancestors.each do |ancestor|
      path << ancestor.name
    end
    path.reverse.join(' / ').html_safe
  end

  def breadcrumb
    crumb = []
    crumb << ["<li class='active'>#{self.name}</li>"]
    ancestors.each do |ancestor|
      crumb << "<li>#{group_link(ancestor.name, url_helpers.group_path(ancestor.id))}</li>"
    end
    crumb.reverse.join('').html_safe
  end

  def ancestors
    ancestors = []
    record = self.parent_group
    while record do
      ancestors << record
      record = record.parent_group
    end
    ancestors
  end

  def group_link(name, id, klass = '')
    ActionController::Base.helpers.link_to name, id, class: klass
  end
end
