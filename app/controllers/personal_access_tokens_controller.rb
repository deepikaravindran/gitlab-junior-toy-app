class PersonalAccessTokensController < ApplicationController
  before_action :logged_in_user, only: [:index, :create, :destroy]

  def index
    @personal_access_token = current_user.personal_access_tokens.new
    @personal_access_tokens = current_user.personal_access_tokens.paginate(page: params[:page])
  end

  def create
    @personal_access_token = current_user.personal_access_tokens.build(token_params)
    @personal_access_token.value = PersonalAccessToken.generate
    if @personal_access_token.save
      flash[:success] = 'New Personal Access Token created'
      redirect_to personal_access_tokens_url
    else
      render 'index', status: :unprocessable_entity
    end
  end

  def destroy
    personal_access_token = PersonalAccessToken.find_by(id: params[:id])
    personal_access_token.destroy
    flash[:success] = "Token deleted"
    if request.referrer.nil?
      redirect_to root_url, status: :see_other
    else
      redirect_to request.referrer, status: :see_other
    end
  end

  private

  def token_params
    params.require(:personal_access_token).permit(:name)
  end
end
