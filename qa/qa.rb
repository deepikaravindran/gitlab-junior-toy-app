require 'zeitwerk'

module QA
  root = "#{__dir__}/qa"

  loader = Zeitwerk::Loader.new

  loader.push_dir(root, namespace: QA)
  loader.ignore("#{root}/specs/features")
  loader.ignore("#{root}/specs/spec_helper.rb")

  loader.inflector.inflect "api" => "API"

  loader.setup
  loader.eager_load
end
