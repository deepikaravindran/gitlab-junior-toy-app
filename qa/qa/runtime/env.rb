module QA
  module Runtime
    module Env
      extend self
      attr_writer :personal_access_token, :admin_personal_access_token

      def app_url
        @app_url ||= ENV['QA_APP_URL'] || 'http://localhost:3000'
      end

      def admin_personal_access_token
        @admin_personal_access_token ||= ENV['QA_ADMIN_ACCESS_TOKEN']
      end

      def personal_access_token
        @personal_access_token ||= ENV['QA_ACCESS_TOKEN']
      end

      def admin_password
        ENV['QA_ADMIN_PASSWORD']
      end

      def admin_email
        ENV['QA_ADMIN_EMAIL']
      end
    end
  end
end
